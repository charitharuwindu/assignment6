#include<stdio.h>

void FibonacciSeq(int x);
int num1=0,num2=1;
int x, num3;

void FibonacciSeq(int x)
{

    if(x>0)
    {
        num3 = num1 + num2;
        num1 = num2;
        num2 = num3;
        printf("\n %d",num3);
        FibonacciSeq(x-1);
    }
}

int main()
{
    printf("Enter the number of rows: ");
    scanf("%d",&x);
    printf("\nFibonacci Sequence: ");
    printf("\n %d \n %d ",0,1);

    FibonacciSeq(x-2);

    return 0;
}
